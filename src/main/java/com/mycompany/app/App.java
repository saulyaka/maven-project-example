package com.mycompany.app;

import java.sql.*;

/**
 * Hello world!
 *
 */
public class App {
    public static void main(String[] args) {
        System.out.println("Hello World!");

        try {
            // Vulnerable code: SQL Injection
            String username = args[0];
            String password = args[1];

            String url = "jdbc:mysql://localhost/mydatabase";
            Connection conn = DriverManager.getConnection(url, username, password);

            // Rest of the code
            // ...
        } catch (SQLException e) {
            System.err.println("Database connection error: " + e.getMessage());
        }
    }
}